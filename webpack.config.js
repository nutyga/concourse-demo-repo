const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const tailwindcss = require("tailwindcss");
const OptimizeCssAssetsWebpackPlugin = require("optimize-css-assets-webpack-plugin");

module.exports = {
	devServer: {
		stats: "errors-only",
		host: process.env.HOST, // Defaults to localhost
		port: process.env.PORT, // Defaults to 8080
		open: true, // Open the page in browser
		writeToDisk: true,
	},
	module: {
		rules: [
			{
				test: /\.pug$/,
				use: [
					{
						loader: "html-loader",
					},
					"pug-html-loader",
				],
			},

			{
				test: /\.(js|jsx)?$/,
				exclude: path.resolve(__dirname, "node_modules"),
				use: [
					{
						// 'babel-loader' enough if we are not using any options
						loader: "babel-loader",
						options: {
							presets: [
								[
									"@babel/preset-env",
									{
										// Instead of '@babel/preset-env'
										targets: {
											browsers: [">= 0.25%", "IE 11", "since 2017", "not op_mini all"],
										},
										useBuiltIns: "usage",
									},
								],
							],
						},
					},
				],
			},
			{
				test: /\.s(a|c)ss$/,
				use: [
					{
						loader: MiniCssExtractPlugin.loader, // Just as adding HTML, we need extracting to be executed last
						options: {
							publicPath: "./",
						},
					},
					"css-loader",
					"postcss-loader",
					"resolve-url-loader", // Get assets that are relative to the scss partial
					{
						loader: "sass-loader",
						options: {
							sourceMap: true,
						},
					},
				],
			},
			{
				test: /\.(jpe?g|png|gif|svg|ico)(\?.*$|$)$/i,
				use: [
					{
						loader: "file-loader",
						options: {
							outputPath: "assets",
							name: "[folder]/[name].[ext]",
							//publicPath: "/assets/images",
						},
					},
				],
			},
			{
				test: /\.(ttf|woff|woff2|eot)$/,
				loader: "file-loader",
				options: {
					name: "[name].[ext]",
					outputPath: "fonts",
					publicPath: "../fonts",
				},
			},
		],
	},
	plugins: [
		new HtmlWebpackPlugin({
			title: "Homepage",
			template: "src/pug-templates/index.pug",
		}),
		new MiniCssExtractPlugin({
			filename: "css/[name].css", // We need to specify at least a file name
		}),
	],
	optimization: {
		minimizer: [new OptimizeCssAssetsWebpackPlugin({})],
	},
};
