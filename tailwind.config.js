const plugin = require("tailwindcss/plugin");

module.exports = {
	// separator: "_",
	// https://tailwindcss.com/docs/configuration#core-plugins
	purge: [],
	darkMode: false, // or 'media' or 'class'
	theme: {
		fontFamily: {
			sans: ["sans-serif"],
			serif: ["serif"],
			"museo-300": "var(--font-museo-300)",
			"museo-500": "var(--font-museo-500)",
			"museo-700": "var(--font-museo-700)",
		},
		colors: {
			// Use https://noeldelgado.github.io/shadowlord/#008000 to generate shades
			"agua-health-green": "var(--agua-health-green)",
			"agua-fabric-green": "var(--agua-fabric-green)",
			"agua-fabric-red": "var(--agua-fabric-red)",
			"agua-red": "var(--agua-red)",
			"agua-silver": "var(--agua-silver)",
			"agua-granite": "var(--agua-granite)",
			"agua-kettleman": "var(--agua-kettleman)",
			"agua-carbon": "var(--agua-carbon)",
			"agua-blue": "var(--agua-blue)",
			"agua-yellow": "var(--agua-yellow)",
			white: "#FFFFFF",
			black: "#000000",
			// These colors are generated from css custom properties
			primary: {
				lighter: "var(--color-primary-lighter)",
				light: "var(--color-primary-light)",
				DEFAULT: "var(--color-primary)",
				dark: "var(--color-primary-dark)",
				darker: "var(--color-primary-darker)",
			},
			accent: {
				lighter: "var(--color-accent-lighter)",
				light: "var(--color-accent-light)",
				DEFAULT: "var(--color-accent)",
				dark: "var(--color-accent-dark)",
				darker: "var(--color-accent-darker)",
			},
			success: {
				lighter: "var(--color-success-lighter)",
				light: "var(--color-success-light)",
				DEFAULT: "var(--color-success)",
				dark: "var(--color-success-dark)",
				darker: "var(--color-success-darker)",
			},
			error: {
				lighter: "var(--color-error-lighter)",
				light: "var(--color-error-light)",
				DEFAULT: "var(--color-error)",
				dark: "var(--color-error-dark)",
				darker: "var(--color-error-darker)",
			},
			warning: {
				lighter: "var(--color-warning-lighter)",
				light: "var(--color-warning-light)",
				DEFAULT: "var(--color-warning)",
				dark: "var(--color-warning-dark)",
				darker: "var(--color-warning-darker)",
			},
			contrast: {
				lighter: "var(--color-contrast-lighter)",
				light: "var(--color-contrast-light)",
				DEFAULT: "var(--color-contrast)",
				dark: "var(--color-contrast-dark)",
				darker: "var(--color-contrast-darker)",
			},
		},
		fontSize: {
			// https://tailwindcss.com/docs/font-size#providing-a-default-line-height
			"14px": ["var(--text-xs)", "var(--leading-xs)"],
			"16px": ["var(--text-sm)", "var(--leading-sm)"],
			"18px": ["var(--text-md)", "var(--leading-md)"],
			"28px": ["var(--text-lg)", "var(--leading-lg)"],
			"34px": ["var(--text-xl)", "var(--leading-xl)"],
			"40px": ["var(--text-xxl)", "var(--leading-xxl)"],
			"54px": ["var(--text-xxxl)", "var(--leading-xxxl)"],
			"72px": ["var(--text-xxxxl)", "var(--leading-xxxxl)"],
		},
		spacing: {
			// https://medium.com/swlh/the-comprehensive-8pt-grid-guide-aa16ff402179
			// Measurements are in pts / pixels
			0: 0,
			1: "1px",
			2: "2px",
			4: "var(--space-xxs)",
			8: "var(--space-xs)",
			16: "var(--space-sm)",
			24: "var(--space-md)",
			32: "var(--space-lg)",
			48: "var(--space-xl)",
			64: "var(--space-xxl)",
			80: "var(--space-xxxl)",
		},
		zIndex: {
			"zindex-header": "var(--zindex-header)",
			"zindex-popover": "var(--zindex-popover)",
			"zindex-fixed-element": "var(--zindex-fixed-element)",
			"zindex-overlay": "var(--zindex-overlay)",
			"zindex-overlay-close-btn": "var(--zindex-overlay-close-btn)",
		},
		screens: {
			xs: "32rem", // ~512px
			sm: "48rem", // ~768px
			md: "64rem", // ~1024px
			lg: "80rem", // ~1280px
			xl: "90rem", // ~1440px
		},
		aspectRatio: {
			// defaults to {}
			none: 0,
			square: [1, 1], // or 1 / 1, or simply 1
			"16/9": [16, 9], // or 16 / 9
			"4/3": [4, 3], // or 4 / 3
			"21/9": [21, 9], // or 21 / 9
		},
		extend: {},
	},
	variants: {
		aspectRatio: ["responsive"],
		extend: {},
	},
	plugins: [require("tailwindcss-aspect-ratio")],
};
